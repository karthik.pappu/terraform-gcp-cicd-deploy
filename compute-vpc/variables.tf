variable "gcp_default_region" {}

variable "gcp_network_name" {
  type        = string
  default     = "terraform-network"
  description = "Default network"
}

variable "gcp_subnet_name" {
  type        = string
  default     = "my-vpc-subnetwork"
  description = "Default subnetwork"
}
