resource "google_compute_network" "vpc_network" {
  name                    = var.gcp_network_name
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "my-subnetwork" {
  name          = var.gcp_subnet_name
  region        = var.gcp_default_region
  ip_cidr_range = "10.127.0.0/20"
  network       = google_compute_network.vpc_network.name
}