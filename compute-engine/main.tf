resource "google_compute_instance" "vm_instance" {
  name         = "terraform-instance"
  machine_type = "f1-micro"
  zone         = var.gcp_default_zone
  boot_disk {
    initialize_params {
      image    = "debian-cloud/debian-9"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network     = var.network_name
    subnetwork  = var.subnetwork_name
  }
}